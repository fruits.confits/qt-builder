FROM ubuntu:16.04

ARG qt_major=5
ARG qt_minor=10
ARG qt_build=1

RUN apt-get update \
  && apt-get install -qqy software-properties-common build-essential g++ git libgl1-mesa-dev

RUN if [ "${qt_minor}" -lt "10" ]; then \
        add-apt-repository ppa:beineri/opt-qt${qt_major}${qt_minor}${qt_build}-$(lsb_release -sc); \
    else \
        add-apt-repository ppa:beineri/opt-qt-${qt_major}.${qt_minor}.${qt_build}-$(lsb_release -sc); \
    fi

RUN apt-get update \
  && apt-get install -qqy qt${qt_major}${qt_minor}-meta-full \
  && rm -rf /var/lib/apt/lists/* \
  && echo ". /opt/qt${qt_major}${qt_minor}/bin/qt${qt_major}${qt_minor}-env.sh" >> /etc/profile

ENV PATH="/opt/qt${qt_major}${qt_minor}/bin:${PATH}"

CMD ["qmake","--version"]
